## A Mayampurath, BA Mitchell
## UChicago
## Program to predict inclusivity using a trained NBC classifier (see output of nbc_classifier.R)
## Inputs 
## 	1. RData file
##	2. Target files containing columns : "Protein_Name,Sequence,Monoisotopic_Mass,Predicted_NET,Detectability"
## 	3. Background flle containing columsn : "Protein_Name,Sequence,Monoisotopic_Mass,Predicted_NET,Detectability"
## Output
##     1. data frame with target peptide, closest background peptide and class label

## --------------------------------------------##

rm(list=ls())

# --- packages --- #
require(dplyr) ; 
require(plyr);
library(e1071);


# --- functions --- #
getNETdiff <- function(net1, net2)
{
  return(abs(net1-net2)) ;
}


# function to calculate mass error
getmassdiff <- function(mass1, mass2, is_ppm = TRUE)
{
  error <- 100 ;
  if (is_ppm)
  {
    error <- (abs(mass1-mass2)/(mass1+mass2)) * 1E06 ;
  }else
  {
    error <- abs(mass1-mass2) ;
  }
  return(error) ;
}

# function to compute log-ratio of detectabilities
getdetectabilityratio <- function(d_t, d_b)
{
  t <-(log(1-d_t)/log(1-d_b));
  if (t==-Inf) { t= 50}
  return(t);
}


min_mass_tol = 10 ;#ppm ; 
min_NET_tol = 0.025 ;
 




# classifier
load("src/nbc/NBC_Classifier.RData");

# input
# Input files
target_file <- " " ; #results/ups/matching/UPS48_AllPeptides_EffectiveDetectabilities.csv" ; 
background_file <- ""; #results/ups/matching/HumanMinusUPS48_AllPeptides_Detectabilities.csv" ;
data.target <- read.csv(target_file, header = TRUE);
data.background <- read.csv(background_file, header = TRUE);


for (i in 1:nrow(data.target)){ 
  cat("Processing peptide:", i, "\n");
  test_target <- data.target[i,];
  # choose background peptide within elution range
  tt<- filter(data.background, 
              getNETdiff(test_target$Predicted_NET, Predicted_NET) < min_NET_tol &
                getmassdiff(test_target$Monoisotopic_Mass, Monoisotopic_Mass) < min_mass_tol);  
  if (nrow(tt) > 0){
    # choose the closest background peptide in terms of mass
    tt <- tt %>% mutate(MassError = getmassdiff(test_target$Monoisotopic_Mass, Monoisotopic_Mass)) 
    choice.background <- tt %>% arrange(desc(MassError)) %>% tail(1); 
    
    this.row <- c(i, test_target$Monoisotopic_Mass, test_target$Predicted_NET, as.character(test_target$Protein_Name), as.character(test_target$Sequence), test_target$Detectability,
                  as.character(choice.background$Protein_Name), as.character(choice.background$Sequence), choice.background$Detectability)
    
    feature_vector <- c(test_target$Monoisotopic_Mass, test_target$Predicted_NET);
    colnames(feature_vector) <- c("Mass", "NET");
    feature_vector["Mass_errror"]= getmassdiff(test_target$Monoisotopic_Mass, choice.background$Monoisotopic_Mass);
    feature_vector["NET_error"]=getNETdiff(test.target$Predicted_NET, choice.background$Predicted_NET);
    feature_vector["der_rat"]=getdetectabilityratio(test.target$Detectability, choice.background$Detectability);    
    
  }else{
    
    # Unique peptide
    this.row <- c(i, test_target$Monoisotopic_Mass, test_target$Predicted_NET, as.character(test_target$Protein_Name), as.character(test_target$Sequence), test_target$Detectability,
                  "NoMatch", "NoMatch", 0);
    feature_vector <- c(test_target$Monoisotopic_Mass, test_target$Predicted_NET);
    colnames(feature_vector) <- c("Mass", "NET");
    feature_vector["Mass_errror"]= 200
    feature_vector["NET_error"]=1
    feature_vector["der_rat"]=50;    
    
    
  }
  p<-predict(t_classifier, feature_vector, "class");
  this.row <- c(this.row, p)
  data.out <- rbind(data.out, this.row);
}




