## -------------------------------------------------------------------------------------------------------------------
## Written by BA Mitchell 
## Program to create a standard set based on maximizing protein coverage
## Input is a matched target-background file with the following headers :
##	IonID,Mass,NET,Target_Protein,Target_Peptide,Target_Detectability,DB_Protein,DB_Peptide,DB_Detectability
## Output is the same format as input with a ClassLabel indicating membership in the standard set 
## --------------------------------------------------------------------------------------------------------------------

import sys
import math
from pymprog import *  # Import the module

# File from which to import data                                                                             
FILE = sys.argv[1]
# Output file name
OUTPUT = sys.argv[2]
# Maximum number of elements allowed on the inclusion list
#MAX_LIST_SIZE = 3000.0
MAX_LIST_SIZE = 300 # AM : for UPS proteins
# Maximum number of elements allowed in a given bucket 
#CAP_B = 75.0
CAP_B= 6 # AM : for UPS proteins
# Maximum bucket size
DELTA = 0.025

#Returns boolean value if string is a numerical value
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

# Create identity matrix with dimensions size x size
def identity(size):
    mat = [[0]*size for i in range(size)]
    for i in range(size):
        mat[i][i] = 1
    for i in range(size):
        mat[i] = tuple(mat[i])
    return mat

# Create matrix corresponding to NET constraints
def make_net_const(net_ind, net_ind_set):
    mat = []
    for ind in net_ind_set:
        curr = []
        for i in range(len(net_ind)):
            if net_ind[i] == ind:
                curr.append(1)
            else:
                curr.append(0)
        mat.append(tuple(curr))
    return mat

#For each value d, compute log(1-d) for use as coefficients in LP objective function
def logify(vals):
    vals_f = [float(v) for v in vals if is_number(v)]
    vals = [-math.log(1-float(v)) for v in vals if is_number(v)]
    return tuple(vals), vals_f

print "Importing and processing data..."
# Pull data from file
f = open(FILE, 'r')
l = f.readlines()

det=[]
net=[]
net_ind=[]
prot=[]
orig=[]
for el in l[1:]:
    n_el = el.strip().split(',')
    if is_number(n_el[2]):
        orig.append(n_el)
        net_ind.append(int(math.floor(float(n_el[2])/DELTA)))
        net.append(n_el[2].strip())
        prot.append((n_el[3].strip(), n_el[4].strip()))
        det.append(n_el[5].strip())
    else:
        continue

#For each detectability d, compute log(1-d) for use as coefficients in LP objective function
det_p = [float(d) for d in det if is_number(d)]
det = [-math.log(1-float(d)) for d in det if is_number(d)]
det = tuple(det)

# Sort values to see rank of resultant selections in inclusion list
det_s = sorted(det, reverse=True)

#Get NET values
net = [el.split(',')[2].strip() for el in l]
#Create bucket indices
net_ind = [int(math.floor(float(n)/DELTA)) for n in net if is_number(n)]
net_ind_set = set(net_ind)
net_const = make_net_const(net_ind, net_ind_set)

#Get peptide/protein pairings
prot = [(el.split(',')[3].strip(), el.split(',')[4].strip()) for el in l[1:]]
prot_pep = {}
prot_pep_ind = {}
for i, p in enumerate(prot):
    prot_pep_ind[p] = i
    if p[0] in prot_pep:
        prot_pep[p[0]].append(i)
    else:
        prot_pep[p[0]] = [i]

# index and data
xid, rid = range(len(det)), range(len(net_const))
c = det

#Constraint that there be less than max_list_size peptides on the inclusion list
mat = [tuple([1.0 for d in det])]
mat1 = net_const

# Dimensionality check before entering LP
assert(len(xid)==len(mat[0]))
assert(len(rid)==len(mat1))
assert(len(xid)==len(mat1[0]))
assert(len(c)==len(xid))

print "Solving linear program..."
#problem definition
beginModel('basic')  
verbose(True)
x = var(xid, 'X', bounds=(0,1)) #create variables
maximize( #set objective
  sum(c[i]*x[i] for i in xid), 'myobj'
)
r=st( #set constraints
  sum(x[j]*mat[0][j] for j in xid) <= MAX_LIST_SIZE 
)
r1=st(
  sum(x[j]*mat1[i][j] for j in xid) <= CAP_B for i in rid
)
solve() #solve and report
print "Solver status:", status()
print 'Z = %g;' % vobj()  # print obj value

# Select peptides included on inclusion list
summ = [(x[i].name, det_s.index(det[i]), det_p[i], net_ind[i], prot[i]) for i in xid if x[i].primal==1]
all_el = [(x[i].name, det_s.index(det[i]), det_p[i], prot[i], x[i].primal) for i in xid]
summ_s = sorted(summ, key=lambda el: el[1])

f = open(OUTPUT, 'w')
l = ['{0},{1}\n'.format(','.join(orig[prot_pep_ind[el[3]]]), el[4]) for el in all_el]
for line in l:
    f.write(line)
f.close()

# Since version 0.3.0
print reportKKT()
print "Environment:", env
for pn in dir(env):
    if pn[:2]=='__'==pn[-2:]: continue
    print pn, getattr(env, pn)
# Since version 0.4.2
print evaluate(sum(x[i]*(i+x[i])**2 for i in xid))
print sum(x[i].primal*(i+x[i].primal)**2 for i in xid)
endModel() #Good habit: do away with the problem

