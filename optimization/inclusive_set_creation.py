## -------------------------------------------------------------------------------------------------------------------
## Written by BA Mitchell 
## Program to create a standard set based on maximizing protein coverage
## Input is a matched target-background file with the following headers :
##      IonID,Mass,NET,Target_Protein,Target_Peptide,Target_Detectability,DB_Protein,DB_Peptide,DB_Detectability
## Output is the same format as input with a ClassLabel indicating membership in the standard set 
## --------------------------------------------------------------------------------------------------------------------
import sys
import math
from pymprog import *  # Import the module

# Import data file
FILE = sys.argv[1]
# Output file name
OUTPUT = sys.argv[2]
# Maximum number of elements allowed on the inclusion list
#MAX_LIST_SIZE =2000.0
MAX_LIST_SIZE = 200.0 # for ups
# Maximum number of elements allowed in a given bucket
#CAP_B = 25.0
CAP_B= 6#for ups
# Maximum bucket size
DELTA = 0.025

#Returns boolean value if string is a numerical value
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

# Create identity matrix with dimensions size x size
def identity(size):
    mat = [[0]*size for i in range(size)]
    for i in range(size):
        mat[i][i] = 1
    for i in range(size):
        mat[i] = tuple(mat[i])
    return mat

# Create matrix corresponding to NET constraints
def make_net_const(net_ind, net_ind_set):
    mat = []
    for ind in net_ind_set:
        curr = []
        for i in range(len(net_ind)):
            if net_ind[i] == ind:
                curr.append(1)
            else:
                curr.append(0)
        mat.append(tuple(curr))
    return mat

# Create matrix where mat[i] is a binary vector indicating which peptides are included in protein i
def make_prot_pep_const(pep, prot_pep):
    mat = []
    for prot in prot_pep:
        curr = []
        for i in range(len(pep)):
            if i in prot_pep[prot]:
                curr.append(1)
            else:
                curr.append(0)
        mat.append(curr)
    return mat    

# Given a list of boolean values, return true if none are true, false otherwise
def test_zero(vals):
    return 1 not in vals

# Given a list of boolean values, return true if at least one value is true, false otherwise
def test_one(vals):
    return 1 in vals

#For each value d, compute log(1-d) for use as coefficients in LP objective function
def logify(vals):
    vals_f = [float(v) for v in vals if is_number(v)]
    vals = [-math.log(1-float(v)) for v in vals if is_number(v)]
    return tuple(vals), vals_f

#Returns smallest Manhattan distance between d and b_det
def min_diff_m(d, b_det):
    return d-min(b_det, key=lambda b_d: d-b_d)

#Returns the smallest Euclidean distance between d and b_det
def min_diff_e(d, b_det):
    return math.sqrt(pow(d-min(b_det, key=lambda b_d: math.sqrt(pow(d-b_d, 2))), 2))

#Returns the smallest squared Euclidean distance between d and b_det
def min_diff_es(d, b_det):
    return pow(d-min(b_det, key=lambda b_d: pow(d-b_d, 2)), 2)

print "Importing and preparing data..."

# Pull data from file
f = open(FILE, 'r')
l = f.readlines()

det=[]
b_det=[]
net=[]
net_ind=[]
prot=[]
orig = []
for el in l[1:]:
    n_el = el.strip().split(',')
    if is_number(n_el[2]):
        orig.append(n_el)
        net_ind.append(int(math.floor(float(n_el[2])/DELTA)))
        net.append(n_el[2].strip())    
        prot.append((n_el[3].strip(), n_el[4].strip()))
        det.append(n_el[5].strip())
        b_det.append(n_el[8].strip())
    else:
        continue

#For each detectability d, compute log(1-d) for use as coefficients in LP objective function
det, det_p = logify(det)
b_det, b_det_p = logify(b_det)

#If a given target peptide does not have a corresponding background peptide (based on mass, NET) then the detectability ratio will simply equal the target detectability
det_rat = []
det_rat_p = []
for i, d in enumerate(det):
    if b_det[i] != 0:
        det_rat.append((d, b_det[i], d/b_det[i]))
    else:
        print ''
        print 'Found unique peptide!!  Are you sure you correctly preprocessed the input file??'
        print ''
        det_rat.append((d, b_det[i], d))        
    det_rat_p.append((det_p[i], b_det_p[i]))
det_rat_obj = [d[2] for d in det_rat]

# Sort values to see rank of resultant selections in inclusion list
det_s = sorted(det, reverse=True)

#Get NET values
net_ind_set = set(net_ind)
net_const = make_net_const(net_ind, net_ind_set)

#Get peptide/protein pairings
prot_pep = {}
prot_pep_ind = {}
for i, p in enumerate(prot):
    prot_pep_ind[p] = i
    if p[0] in prot_pep:
        prot_pep[p[0]].append(i)
    else:
        prot_pep[p[0]] = [i]    
z_const = make_prot_pep_const(prot, prot_pep)

# index and data
xid, rid = range(len(det_rat_obj)), range(len(net_const))
c = det_rat_obj[:]

#Constraint that there be less than max_list_size peptides on the inclusion list
mat = [tuple([1.0 for d in det_rat_obj])]# + identity(len(det))
mat1 = net_const

print "Solving linear program..."
# Dimensionality checks on the matrices and arrays to be used in the LP
assert(len(xid)==len(mat[0]))
assert(len(rid)==len(mat1))
assert(len(xid)==len(mat1[0]))
assert(len(c)==len(xid))

# problem definition
beginModel('basic')  
verbose(True)
# create variables
x = var(xid, 'X', bounds=(0,1)) 
# set objective
maximize( 
  sum(c[i]*x[i] for i in xid), 'myobj'
)
# set constraints
r=st( 
  sum(x[j]*mat[0][j] for j in xid) <= MAX_LIST_SIZE 
)
r1=st(
  sum(x[j]*mat1[i][j] for j in xid) <= CAP_B for i in rid
)
solve() 

print "Dumping solver results..."
print "Solver status:", status()
print 'Z = %g;' % vobj()  # print obj value

# Select peptides included on inclusion list
summ = [(x[i].name, det_s.index(det[i]), det_p[i], net_ind[i], prot[i]) for i in xid if x[i].primal==1]
all_el = [(x[i].name, det_s.index(det[i]), det_p[i], prot[i], x[i].primal) for i in xid]
summ_s = sorted(summ, key=lambda el: el[1])

f = open(OUTPUT, 'w')
l = ['{0},{1}\n'.format(','.join(orig[prot_pep_ind[el[3]]]), el[4]) for el in all_el]
for line in l:
    f.write(line)
f.close()

# Print peptides on inclusion list
#print ';\n'.join('%s, rank: %i, detectability: %f, NET: %f, Protein: %s, Peptide: %s' % (el[0], el[1], el[2], el[3], el[4][0], el[4][1]) for el in summ_s)

# Since version 0.3.0
print reportKKT()
print "Environment:", env
for pn in dir(env):
    if pn[:2]=='__'==pn[-2:]: continue
    print pn, getattr(env, pn)
# Since version 0.4.2
print evaluate(sum(x[i]*(i+x[i])**2 for i in xid))
print sum(x[i].primal*(i+x[i].primal)**2 for i in xid)
endModel() #Good habit: do away with the problem

f.close()
